import React from 'react';
import logo from './logo.svg';
import './App.css';
import FullCalendar from '@fullcalendar/react'
import dayGridPlugin from '@fullcalendar/daygrid'
import interactionPlugin from "@fullcalendar/interaction"; // needed for dayClick
import DatePicker, { DateInput, TimeInput } from '@trendmicro/react-datepicker';
import '@trendmicro/react-datepicker/dist/react-datepicker.css';
import * as moment from 'moment';

export default class App extends React.Component {
  state = {
    date: moment().format('YYYY-MM-DD'),
    showDatePicker: false,
    events: [
      { id: '10', title: 'event 1', start: '2020-07-01', end: '2020-07-04' },
      { id: '12', title: 'event 2', start: '2020-07-06', end: '2020-07-10' }
    ]
  };
  render() {
    return (
      <>
        <div className="date-picker-component">
          <input
            value={this.state.date}
            type="text"
            onFocus={() => {
              this.setState({ showDatePicker: true })
            }} /> <br />
          {
            this.state.showDatePicker &&
            <div className="date-picker-container">
              <DatePicker
                date={this.state.date}
                onSelect={date => {
                  this.setState(state => ({ date: date }), () => {
                    this.setState({ showDatePicker: false })
                  });
                }}
              />
            </div>
          }
        </div>

        <FullCalendar
          plugins={[dayGridPlugin, interactionPlugin]}
          dateClick={this.handleDateClick}
          eventClick={this.handleEventClick}
          events={this.state.events}
        />
      </>
    )
  }

  handleDateClick = (arg) => { // bind with an arrow function
    const {events} = this.state;
    const newEvent = {title: 'event 1', date: arg.dateStr};
    this.setState({date: arg.dateStr, events: events.concat(newEvent)})
  }

  handleEventClick = (arg) => { // bind with an arrow function
    console.log(arg.event.title);
  }

}
